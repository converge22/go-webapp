module gitlab.com/converge22/go-webapp

go 1.18

require (
	github.com/stretchr/testify v1.5.1
	golang.org/x/mod v0.3.0
)
