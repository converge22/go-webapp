job "demo-[[.environment_slug]]" {
  type = "service"
  datacenters = ["asia-east1-b"]
  meta {
    git_sha = "[[.git_sha]]"
  }
  group "demo" {
    count = 1

    task "server" {
      env {
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      }

      driver = "exec"
   
      artifact {
        source = "[[.artifact_url]]"
      }

      config {
        command = "demo-webapp"
      }

      resources {
        cpu = 128
        memory = 128
        network {
          mbits = 10
          port  "http"{}
        }
      }

      service {
        name = "review-[[.environment_slug]]"
        port = "http"

        tags = [
          "traefik.enable=true",
          "traefik.http.routers.review-demo-https.tls=true",
          "traefik.http.routers.review-demo-https.rule=Host(`[[.deploy_url]]`)",
          "traefik.http.routers.review-demo-https.tls.domains[0].main=[[.deploy_url]]",
          "traefik.http.routers.review-demo-http.rule=Host(`[[.deploy_url]]`)",
          "traefik.http.routers.review-demo-https.tls.certresolver=myresolver"
        ]

        check {
          type = "http"
          path = "/"
          interval = "5s"
          timeout = "1s"
        }
      }

    }
  }
}

