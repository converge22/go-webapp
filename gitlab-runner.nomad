job "nomad-runner" {
  
  datacenters = ["asia-east1-c"]

   task "gitlab-runner" {
     driver = "docker"

	  resources {
        cpu    = 1000
        memory = 2000
      }

      config {
        image = "gitlab/gitlab-runner:latest"
        volumes=["/var/run/docker.sock:/var/run/docker.sock"]
        network_mode = "host"
      }

      service {
        name = "gitlab-runner"
        tags = [
          "docker",
          "gitlab-runner"
        ]
      }
    }
  }