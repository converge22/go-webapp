job "demo" {
  type = "service"
  datacenters = ["asia-east1-b"]
  meta {
    git_sha = "[[.git_sha]]"
  }
  group "demo" {
    count = 2

    update {
      max_parallel = 2
      canary = 2
    }

    task "server" {
      env {
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      }

      driver = "exec"
   
      artifact {
        source = "[[.artifact_url]]"
      }

      config {
        command = "demo-webapp"
      }

      resources {
        cpu = 128
        memory = 128
        network {
          mbits = 10
          port  "http"{}
        }
      }

      service {
        name = "demo-canary"
        port = "http"

        tags = []
        canary_tags = [
          "traefik.enable=true",
          "traefik.http.routers.canary-demo-https.tls=true",
          "traefik.http.routers.canary-demo-https.rule=Host(`canary.demo.brbb.cloud`)",
          "traefik.http.routers.canary-demo-https.tls.domains[0].main=canary.demo.brbb.cloud",
          "traefik.http.routers.canary-demo-http.rule=Host(`canary.demo.brbb.cloud`)",
          "traefik.http.routers.canary-demo-https.tls.certresolver=myresolver"
        ]

        check {
          type = "http"
          path = "/"
          interval = "5s"
          timeout = "1s"
        }
      }

      service {
        name = "demo"
        port = "http"

        tags = [
          "traefik.enable=true",
          "traefik.http.routers.prod-demo-https.tls=true",
          "traefik.http.routers.prod-demo-https.rule=Host(`demo.brbb.cloud`)",
          "traefik.http.routers.prod-demo-https.tls.domains[0].main=demo.brbb.cloud",
          "traefik.http.routers.prod-demo-http.rule=Host(`demo.brbb.cloud`)",
          "traefik.http.routers.prod-demo-https.tls.certresolver=myresolver"
        ]
        canary_tags = [
          "traefik.enable=false"
        ]

        check {
          type = "http"
          path = "/"
          interval = "5s"
          timeout = "1s"
        }
      }

    }
  }
}

